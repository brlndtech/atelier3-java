import { applyMiddleware, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore } from 'redux-persist';
import { flashReset } from '../actions';
import { FLASH_DISPLAY } from '../actions/actions';

import rootReducer from '../reducers'

const middleWareFlashReset = ({ dispatch }) => {
    return next => action => {
      if (action.type === FLASH_DISPLAY) {
        setTimeout(() => {
          dispatch(flashReset())
        }, 5000)
      }
      const returnedValue = next(action)
      return returnedValue
    }
}

const middlewares = [middleWareFlashReset]
const middlewareEnhancer = applyMiddleware(...middlewares)

const enhancers = [middlewareEnhancer]
const composedEnhancers = composeWithDevTools(...enhancers)

const store = createStore(rootReducer, undefined, composedEnhancers)

const persistor = persistStore(store)

export { persistor, store }