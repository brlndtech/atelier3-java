import React from 'react';
import { connect } from 'react-redux'

import { CardItem } from '.';
import { flashDisplay } from '../actions';
import { buy as apiBuy, getUser } from '../api';
import { Button, Div } from '../components';

class CardItemBuy extends React.Component {
    constructor(props) {
        super(props);

        this.buy = this.buy.bind(this);
    }

    buy(cardId) {
        const { dispatchFlashDisplay, update, updateUser, user } = this.props;

        apiBuy(cardId, user, (data) => {
            if (data === true) {
                update();
                getUser(user.id, (data) => {
                    if (data !== 'Error') {
                        updateUser(data);
                    }
                });
                dispatchFlashDisplay({ msg: `Vous avez bien acheté la carte`, success: true })
            } else {
                dispatchFlashDisplay({ msg: `Erreur dans l'achat de la carte`, success: false })
            }
        })
    }

    render() {
        const { card, user } = this.props;

        return (
            <Div vertical mr="10px">
                <Div w="320px" center>
                    {`Propriétaire : ${card.user.id === user.id ? 'Vous' : card.user.username}`}
                </Div>
                <CardItem card={card} />
                <Div w="320px">
                    <Button disabled={card.user.id === user.id} w="100%" label={`Acheter la carte (${card.price}€)`} onClick={() => this.buy(card.id)} />
                </Div>
            </Div>
        )
    }
}

const mapDispatchToProps = {
    dispatchFlashDisplay: flashDisplay,
}

export default connect(null, mapDispatchToProps)(CardItemBuy);
