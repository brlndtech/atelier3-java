import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'

import { Div, FlashMessage } from '../components'
import { CardList, CombatList, Footer, Inscription, Navbar, Welcome } from '.';
import { createUser, flashReset } from '../actions';
import { getUser } from '../api';

const Cartes = () => <CardList market={false} />
const Market = () => <CardList market={true} />

class Router extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            redirect: null,
            visible: false,
        }
    }

    componentDidMount() {
        const { dispatchCreateUser, init, items } = this.props;

        if (init) {
            getUser(items.id, (data) => {
                if (data !== 'Error') {
                    dispatchCreateUser(data);
                }
            });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const { initFlash } = this.props;
        const { visible } = this.state;

        if (prevProps.initFlash !== initFlash) {
            setTimeout(() => this.setState({
                visible: !visible
            }), visible ? 300 : 0)
        }
    }

    render() {
        const { dispatchFlashReset, init, initFlash, itemsFlash } = this.props;
        const { visible } = this.state;

        return (
            <BrowserRouter>
                <Navbar />
                    <Div mt="50px" p="50px">
                        <Switch>
                            {init && (
                                <React.Fragment>
                                    <Route path="/cartes" component={Cartes} />
                                    <Route path="/marche" component={Market} />
                                    <Route path="/combats" component={CombatList} />
                                </React.Fragment>
                            )}
                            <Route path="/inscription" component={Inscription} />
                            <Route path="/" component={Welcome} />
                        </Switch>
                    </Div>
                <FlashMessage
                    msg={initFlash ? itemsFlash.msg : null}
                    animateIn={initFlash}
                    onClick={dispatchFlashReset}
                    success={initFlash ? itemsFlash.success : null}
                    visible={visible}
                />
                <Footer />
            </BrowserRouter>
        );
    }
}
const mapStateToProps = ({ flash: { init: initFlash, items: itemsFlash }, user: { init, items } }) => ({
    init,
    initFlash,
    items,
    itemsFlash
});

const mapDispatchToProps = {
    dispatchCreateUser: createUser,
    dispatchFlashReset: flashReset,
}

export default connect(mapStateToProps, mapDispatchToProps)(Router);
