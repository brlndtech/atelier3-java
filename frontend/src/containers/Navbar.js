import React from 'react';
import { Redirect, withRouter } from "react-router-dom";
import { connect } from 'react-redux';

import { createCards, createUser, deleteUser, flashDisplay } from '../actions'
import { connect as apiConnect, getCards } from '../api';
import { Button, Input, Div } from '../components';

class Navbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            redirect: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.redirect = this.redirect.bind(this);
    }

    handleChange(e) {
        let input = {};
        input[e.target.placeholder.toLowerCase()] = e.target.value;
        this.setState(input);
    }

    handleSubmit() {
        const { dispatchCreateCards, dispatchCreateUser, dispatchFlashDisplay } = this.props;
        const { username, password } = this.state;

        apiConnect({ username, password }, (data) => {
            if (data) {
                dispatchCreateUser(data);
                getCards(data.id, (data) => {
                    if (data !== 'Error') {
                        dispatchCreateCards(data);
                    }
                });
                dispatchFlashDisplay({ msg: `Vous êtes connecté en tant que ${data.username}`, success: true })
            } else {
                dispatchFlashDisplay({ msg: `Identifiant ou mot de passe incorrect`, success: false })
            }
        })
    }

    redirect(redirection) {
        this.setState({
            redirect: redirection
        }, () => this.setState({
            redirect: ''
        }));
    }

    render() {
        const { dispatchDeleteUser, location: { pathname }, init, items } = this.props;
        const { username, password, redirect } = this.state;

        if (redirect) {
            return <Redirect to={redirect} />
        }

        return (
            <Div bc="#404040" c="#cecece" h="50px" t="0" w="100%" fixed>
                <Div w="calc(100% / 3)">
                    <Div w="calc(100% / 3)" center middle borderBottom={pathname === '/cartes'} borderBottomHover onClick={() => this.redirect('cartes')}>Mes cartes</Div>
                    <Div w="calc(100% / 3)" center middle borderBottom={pathname === '/marche'} borderBottomHover onClick={() => this.redirect('marche')}>Le marché</Div>
                    <Div w="calc(100% / 3)" center middle borderBottom={pathname === '/combats'} borderBottomHover onClick={() => this.redirect('combats')}>Mes combats</Div>
                </Div>
                <Div w="calc(100% / 3)" center middle>Jeu de cartes</Div>
                {init ? (
                    <Div w="calc(100% / 3)" middle right>
                        <Div mr="2%">{`Bonjour, ${items.username} ! Vous avez ${items.money}€`}</Div>
                        <Button label="Déconnexion" onClick={dispatchDeleteUser}/>
                    </Div>
                ) : (
                    <Div w="calc(100% / 3)">
                        <Input type="text" placeholder="Username" onChange={this.handleChange} value={username} />
                        <Input type="password" placeholder="Password" onChange={this.handleChange} value={password} />
                        <Button label="Se connecter" onClick={this.handleSubmit} />
                        <Button label="Inscription" onClick={() => this.redirect('inscription')}/>
                    </Div>
                )}
            </Div>
        )
    }
}

const mapStateToProps = ({ user: { init, items } }) => ({
    init,
    items,
});

const mapDispatchToProps = {
    dispatchCreateCards: createCards,
    dispatchCreateUser: createUser,
    dispatchDeleteUser: deleteUser,
    dispatchFlashDisplay: flashDisplay,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navbar));