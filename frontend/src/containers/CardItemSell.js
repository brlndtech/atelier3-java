import React from 'react';
import { connect } from 'react-redux'

import { CardItem } from '.'
import { flashDisplay } from '../actions';
import { cancelSell, getUser, sell, subscribe, unsubscribe } from '../api';
import { Button, Div, Input } from '../components';

class CardItemSell extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            bet: '',
            price: ''
        }

        this.cancelSell = this.cancelSell.bind(this);
        this.handleChangeBet = this.handleChangeBet.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSubscribe = this.handleSubscribe.bind(this);
        this.handleUnSubscribe = this.handleUnSubscribe.bind(this);
    }

    cancelSell() {
        const { card, dispatchFlashDisplay, update } = this.props;

        cancelSell(card, (data) => {
            if (data !== 'Error') {
                update();
                dispatchFlashDisplay({ msg: `Vous avez annulé la vente de votre carte`, success: true })
            } else {
                dispatchFlashDisplay({ msg: `Erreur dans l'annulation de la vente de votre carte`, success: false })
            }
        })
    }

    handleChangeBet(e) {
        this.setState({
            bet: e.target.value
        })
    }

    handleChangePrice(e) {
        this.setState({
            price: e.target.value
        })
    }

    handleSubmit() {
        const { card, dispatchFlashDisplay, update } = this.props;
        const { price } = this.state;

        sell(price, card, (data) => {
            if (data !== 'Error') {
                update();
                dispatchFlashDisplay({ msg: `Vous avez mis en vente votre carte`, success: true })
            } else {
                dispatchFlashDisplay({ msg: `Erreur dans la mise en vente de votre carte`, success: false })
            }
        })
    }

    handleSubscribe(cardId) {
        const { dispatchFlashDisplay, update, updateUser, user } = this.props;
        const { bet } = this.state;

        subscribe(cardId, bet, (data) => {
            if (data !== 'Error') {
                update();
                getUser(user.id, (uData) => {
                    if (uData !== 'Error') {
                        updateUser(uData);
                    }
                });
                this.setState({
                    bet: ''
                });
                dispatchFlashDisplay({ msg: `Vous êtes bien inscris au combat avec une mise de ${bet}€`, success: true })
            } else {
                dispatchFlashDisplay({ msg: `Erreur dans l'inscription au combat`, success: false })
            }
        });
    }

    handleUnSubscribe(cardId) {
        const { dispatchFlashDisplay, update } = this.props;

        unsubscribe(cardId, (data) => {
            if (data !== 'Error') {
                update();
                dispatchFlashDisplay({ msg: `Vous avez bien annulé votre inscription au combat`, success: true })
            } else {
                dispatchFlashDisplay({ msg: `Erreur dans l'annulation de votre inscription au combat`, success: true })
            }
        })
    }

    render() {
        const { card } = this.props;
        const { bet, price } = this.state;

        return (
            <Div vertical mr="10px">
                <CardItem card={card} />
                <Div w="320px">
                    {card.selling ? (
                        <Button w="100%" label={`Annuler la vente (${card.price}€)`} onClick={this.cancelSell} />
                    ) : (
                        <React.Fragment>
                            <Input w="50%" type="number" min="0" placeholder="Prix" onChange={this.handleChangePrice} value={price} />
                            <Button disabled={card.inRoom || !(price && price.length !== 0)} w="50%" label="Mettre en vente" onClick={this.handleSubmit} />
                        </React.Fragment>
                    )}
                </Div>
                <Div w="320px">
                    {card.inRoom ? (
                        <Button disabled={card.energy < 20} w="100%" label="Annuler le combat" onClick={() => this.handleUnSubscribe(card.id)} />
                    ) : (
                        <React.Fragment>
                            <Input w="50%" type="number" min="0" placeholder="Mise" onChange={this.handleChangeBet} value={bet} />
                            <Button disabled={card.selling || !(bet && bet.length !== 0) || card.energy < 20} w="50%" label="⚔ Combattre ⚔" onClick={() => this.handleSubscribe(card.id)} />
                        </React.Fragment>
                    )}
                </Div>
            </Div>
        )
    }
}

const mapDispatchToProps = {
    dispatchFlashDisplay: flashDisplay,
}

export default connect(null, mapDispatchToProps)(CardItemSell);
