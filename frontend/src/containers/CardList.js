import React from 'react';
import { connect } from 'react-redux';

import { createCards, createMarketCards, createUser } from '../actions';
import { getCards, getSellingCards } from '../api';
import { CardItemBuy, CardItemSell } from '.';
import { Div } from '../components';

class CardList extends React.Component {
    constructor(props) {
        super(props);

        this.refreshCards = this.refreshCards.bind(this);
    }

    componentDidMount() {
        this.refreshCards();
    }

    refreshCards() {
        const { dispatchCreateCards, dispatchCreateMarketCards, initUser, itemsUser, market } = this.props;

        if (initUser) {
            if (market) {
                getSellingCards((data) => {
                    if (data !== 'Error') {
                        dispatchCreateMarketCards(data);
                    }
                })
            } else {
                getCards(itemsUser.id, (data) => {
                    if (data !== 'Error') {
                        dispatchCreateCards(data);
                    }
                })
            }
        }
    }

    render() {
        const { dispatchCreateUser, init, initMarket, items, itemsMarket, itemsUser, market } = this.props;

        if ((!market && !init) || (market && !initMarket)) {
            return (
                <Div c="#cecece" w="100%" center middle vertical>
                    Loading ...
                </Div>
            )
        }

        if (market && itemsMarket.length === 0) {
            return (
                <Div c="#cecece" w="100%" center middle vertical>
                    Le marché est vide
                </Div>
            )
        }

        const cardList = [];
        for (let i = 0; i < Math.ceil((market ? itemsMarket.length : items.length) / 5); i++) {
            const cardListLine = [];
            for (let j = 0; j < 5; j++) {
                if (market && itemsMarket[i * 5 + j]) {
                    cardListLine.push(itemsMarket[i * 5 + j]);
                }
                if (!market && items[i * 5 + j]) {
                    cardListLine.push(items[i * 5 + j]);
                }
            }
            cardList.push(cardListLine);
        }

        return (
            <Div vertical w="100%">
                {cardList.map(cardListLine => (
                    <Div key={cardListLine} c="#cecece" w="100%" center middle>
                        {!market ? cardListLine.map((card) => (
                            <CardItemSell key={`carditem${card.id}`} card={card} update={this.refreshCards} updateUser={dispatchCreateUser} user={itemsUser} />
                        )) : cardListLine.map((card) => (
                            <CardItemBuy key={`carditem${card.id}`} card={card} update={this.refreshCards} updateUser={dispatchCreateUser} user={itemsUser} />
                        ))}
                    </Div>
                ))}
            </Div>
            
        )
    }
}

const mapStateToProps = ({ card: { init, items }, market: { init: initMarket, items: itemsMarket }, user: { init: initUser, items: itemsUser } }) => ({
    init,
    initMarket,
    initUser,
    items,
    itemsMarket,
    itemsUser,
});

const mapDispatchToProps = {
    dispatchCreateCards: createCards,
    dispatchCreateMarketCards: createMarketCards,
    dispatchCreateUser: createUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(CardList);
