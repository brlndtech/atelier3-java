import React from 'react';
import styled from 'styled-components';

const Button = styled.button`
    outline: none;
    height: 100%;
    border: solid 2px ${({ disabled }) => disabled ? '#303030' : '#425c49'};
    background-color: ${({ disabled }) => disabled ? '#303030' : '#cecece'};
    color: ${({ disabled }) => disabled ? '#101010' : '#425c49'};
    font-size: 18px;
    transition: 0.4s;
    width: calc(100% / 4);
    border-radius: 15px;
    ${({ w }) => w && `width: ${w};`}

    :hover {
        ${({ disabled }) =>  `
            cursor: ${disabled ? 'default' : 'pointer'};
            background-color: ${disabled ? '#303030' : '#425c49'};
            color: ${disabled ? '#101010' : '#cecece'};
        `}
    }
`;

const StyledButton = ({ disabled, label, onClick, w }) => <Button disabled={disabled} onClick={onClick} w={w}>{label}</Button>

export default StyledButton;