import React from 'react';
import styled from 'styled-components';

const Input = styled.input`
    position: relative;
    border: none;
    background-color: transparent;
    text-decoration: none;
    outline: none;
    border-bottom: solid 3px transparent;
    font-size: 18px;
    color: #cecece;
    margin-top: 3px;
    height: calc(100% - 9px);
    width: calc(100% / 4);
    transition: 0.4s;
    ${({ w }) => w && `width: ${w};`}

    :focus {
        border: none;
        outline: none;
        border-bottom: solid 3px #cecece;
    }
`;

const StyledInput = ({ w, ...otherProps }) => <Input w={w} { ...otherProps } />

export default StyledInput;