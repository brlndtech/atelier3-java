const API_URL = "api/";

export const get = (api, handler) => {
    fetch(API_URL + api, {
        'method': 'GET'
    })
    .then((res) => {
        switch (res.status) {
            case 200:
                return res.json()
            case 404:
            default:
                return "Error"
        }
    })
    .catch()
    .then(handler)
    .catch()
}

export const post = (api, body, handler) => {
    fetch(API_URL + api, {
        'headers': {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'method': 'POST',
        'body': JSON.stringify(body)
    })
    .then((res) => {
        switch (res.status) {
            case 200:
                const contentType = res.headers.get("content-type");
                if (contentType && contentType.indexOf("application/json") !== -1) {
                    return res.json();
                  } else {
                    return res.text();
                  }
            case 404:
            default:
                return "Error"
        }
    })
    .catch()
    .then(handler)
    .catch()
}