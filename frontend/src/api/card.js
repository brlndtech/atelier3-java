import { get } from './methods';

export const getSellingCards = callback => get("card/selling", callback);