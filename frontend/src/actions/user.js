import { CREATE_USER, DELETE_USER } from './actions'

export const createUser = (user) => ({
    type: CREATE_USER,
    payload: user,
});

export const deleteUser = () => ({
    type: DELETE_USER,
    payload: null,
})