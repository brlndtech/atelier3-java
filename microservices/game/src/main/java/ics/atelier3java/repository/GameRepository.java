package ics.atelier3java.repository;

import org.springframework.data.repository.CrudRepository;

import ics.atelier3java.model.Game;

public interface GameRepository extends CrudRepository<Game, Integer> {

}
