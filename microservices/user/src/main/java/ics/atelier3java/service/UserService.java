package ics.atelier3java.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ics.atelier3java.common.dto.CardDTO;
import ics.atelier3java.model.User;
import ics.atelier3java.repository.UserRepository;

@RestController
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	public User get(int userId) {
		Optional<User> oUser = userRepository.findById(userId);
		if (oUser.isPresent()) {
			return oUser.get();
		} else {
			return null;
		}
	}
	
	public User update(User user) {
		return userRepository.save(user);
	}
	
	public User createUser(User user) {
		User newUser = userRepository.save(user);
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    ObjectMapper mapper = new ObjectMapper();
	    String jsonInString = "";
		try {
			jsonInString = mapper.writeValueAsString(user);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		HttpEntity<String> request = new HttpEntity<String>(jsonInString, headers);
		ResponseEntity<String> response = restTemplate.postForEntity("http://card:8080/random/5", request, String.class);
		ObjectMapper objectMapper = new ObjectMapper();
		List<CardDTO> cards = null;
		try {
			cards = objectMapper.readValue(response.getBody(), new TypeReference<List<CardDTO>>(){});
			if (cards != null) {
				for (CardDTO card : cards) {
					restTemplate.postForEntity("http://usercard:8080/" + card.getId() + "/" + user.getId(), request, String.class);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		newUser.setMoney(100);
		return userRepository.save(newUser);
	}
	
	public List<User> getUsers() {
		List<User> userList = new ArrayList<User>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}
	
	public User getUserByName(String name) {
		Optional<User> oUser = userRepository.findByUsername(name);
		if (oUser.isPresent()) {
			return oUser.get();
		} else {
			return null;
		}
	}
	
	public User getUserById(int id) {
		Optional<User> oUser = userRepository.findById(id);
		if (oUser.isPresent()) {
			return oUser.get();
		} else {
			return null;
		}
	}
}
