package ics.atelier3java.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ics.atelier3java.common.dto.UserDTO;
import ics.atelier3java.model.User;
import ics.atelier3java.service.UserService;

@RestController
@RequestMapping()
public class UserRest {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/{userId}")
	public UserDTO get(@PathVariable int userId) {
		User user = userService.get(userId);
		if (user != null) {
			return user.toDTO();
		} else {
			return null;
		}
	}
	
	@PutMapping("/{userId}")
	public UserDTO update(@RequestBody User user) {
		User uUser = userService.update(user);
		if (uUser != null) {
			return uUser.toDTO();
		} else {
			return null;
		}
	}
	
	@PostMapping("/create")
	public UserDTO createUser(@RequestBody User user) {
		User rUser = userService.createUser(user);
		System.out.println(rUser);
		if (rUser != null) {
			return rUser.toDTO();
		} else {
			return null;
		}
	}
	
	@GetMapping("/list")
	public List<UserDTO> getUsers() {
		List<UserDTO> users = new ArrayList<UserDTO>();
		for (User user : userService.getUsers()) {
			users.add(user.toDTO());
		}
		return users;
	}
	
	@GetMapping("/findOne/{username}")
	public UserDTO getUserByName(@PathVariable String username) {
		if (userService.getUserByName(username) != null) {
			return userService.getUserByName(username).toDTO();
		} else {
			return null;
		}
	}
}
