package ics.atelier3java;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.nio.charset.Charset;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import ics.atelier3java.common.dto.UserDTO;
import ics.atelier3java.rest.UserRest;
import ics.atelier3java.service.UserService;

@WebMvcTest(controllers = UserRest.class)
class UserApplicationTests {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private UserService userService;
	
	private UserDTO user = null;
	
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
	
	public UserApplicationTests() {
		user = new UserDTO();
		user.setPrenom("Lorem");
		user.setNom("Ipsum");
		user.setUsername("LorIps");
		user.setPassword("emum");
	}
	
	@Test
	public void testCreateUser() throws Exception {
		String userString = "{\"username\":\"" + user.getUsername() + "\","
				+ "\"password\":\"" + user.getPassword() + "\","
				+ "\"nom\":\"" + user.getNom() + "\","
				+ "\"prenom\":\"" + user.getPrenom() + "\"}";
		mockMvc.perform(post("/create")
				.accept(MediaType.APPLICATION_JSON)
				.content(userString)
				.contentType(MediaType.APPLICATION_JSON)
			)
			.andDo(print())
			.andExpect(status().isOk());
	}
	
	@Test
	public void testGetUser() throws Exception {
		mockMvc.perform(get("/1"))
			.andExpect(status().isOk());
	}
	
	@Test
	public void testGetUserList() throws Exception {
		mockMvc.perform(get("/list"))
			.andExpect(status().isOk());
	}

}
