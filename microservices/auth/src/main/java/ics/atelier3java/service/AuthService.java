package ics.atelier3java.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ics.atelier3java.common.dto.UserDTO;

@RestController
public class AuthService {
	
	public UserDTO connection(UserDTO user) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity("http://user:8080/findOne/" + user.getUsername(), String.class);
		UserDTO fUser = null;
		if (response.getBody() != null && !response.getBody().equals("null")) {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				fUser = objectMapper.readValue(response.getBody(), new TypeReference<UserDTO>(){});
				if (fUser != null && !(fUser.getUsername().equals(user.getUsername()) && fUser.getPassword().equals(user.getPassword()))) {
					fUser = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return fUser;
	}
}

