package ics.atelier3java.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import ics.atelier3java.model.UserCard;

public interface UserCardRepository extends CrudRepository<UserCard, Integer> {
	
	List<UserCard> findByUserId(int userId);

	Optional<UserCard> findByCardId(int cardId);

}
