package ics.atelier3java.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ics.atelier3java.common.dto.CardDTO;
import ics.atelier3java.common.dto.UserDTO;
import ics.atelier3java.model.UserCard;
import ics.atelier3java.service.UserCardService;

@RestController
@RequestMapping()
public class UserCardRest {
	
	@Autowired
	private UserCardService userCardService;
	
	@PostMapping("/{cardId}/{userId}")
	public boolean createUserCard(@PathVariable int cardId, @PathVariable int userId) {
		return userCardService.createUserCard(cardId, userId);
	}
	
	@GetMapping("/user/{userId}")
	public List<CardDTO> getCardsByUser(@PathVariable int userId) {
		return userCardService.getCardsByUser(userId);
	}

	@GetMapping("/card/{cardId}")
	public UserDTO getUserByCard(@PathVariable int cardId) {
		return userCardService.getUserByCard(cardId);
	}
	
	@GetMapping("/")
	public List<UserCard> getAll() {
		return userCardService.getAll();
	}
	
}
