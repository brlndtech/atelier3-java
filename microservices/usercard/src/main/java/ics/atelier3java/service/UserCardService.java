package ics.atelier3java.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ics.atelier3java.common.dto.CardDTO;
import ics.atelier3java.common.dto.UserDTO;
import ics.atelier3java.model.UserCard;
import ics.atelier3java.repository.UserCardRepository;

@RestController
public class UserCardService {
	
	@Autowired
	private UserCardRepository userCardRepository;
	
	public boolean createUserCard(int cardId, int userId) {
		UserCard userCard = new UserCard(userId, cardId);
		UserCard userCardExist = null;
		Optional<UserCard> oUserCard = userCardRepository.findByCardId(cardId);
		if (oUserCard.isPresent()) {
			userCardExist = oUserCard.get(); 
		}
		if (userCardExist != null) {
			userCardRepository.delete(userCardExist);
		}
		return userCardRepository.save(userCard) != null;
	}
	
	public List<CardDTO> getCardsByUser(int userId) {
		List<UserCard> userCardList = new ArrayList<UserCard>();
		userCardRepository.findByUserId(userId).forEach(userCardList::add);
		RestTemplate restTemplate = new RestTemplate();
		List<CardDTO> cards = new ArrayList<CardDTO>();
		for (UserCard userCard : userCardList) {
			ResponseEntity<String> response = restTemplate.getForEntity("http://card:8080/" + userCard.getCardId(), String.class);
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				CardDTO card = objectMapper.readValue(response.getBody(), new TypeReference<CardDTO>(){});
				if (card != null) {
					cards.add(card);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return cards;
	}

	public UserDTO getUserByCard(int cardId) {
		UserCard userCard = null;
		Optional<UserCard> oUserCard = userCardRepository.findByCardId(cardId);
		if (oUserCard.isPresent()) {
			userCard = oUserCard.get(); 
		}
		if (userCard != null) {
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.getForEntity("http://user:8080/" + userCard.getUserId(), String.class);
			ObjectMapper objectMapper = new ObjectMapper();
			UserDTO user = null;
			try {
				user = objectMapper.readValue(response.getBody(), new TypeReference<UserDTO>(){});
			} catch (Exception e) {
				e.printStackTrace();
			}
			return user;
		}
		return null;
	}
	
	public List<UserCard> getAll() {
		List<UserCard> userCardList = new ArrayList<UserCard>();
		userCardRepository.findAll().forEach(userCardList::add);
		return userCardList;
	}

}
