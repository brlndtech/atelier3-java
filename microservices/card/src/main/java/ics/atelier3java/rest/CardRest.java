package ics.atelier3java.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ics.atelier3java.model.Card;
import ics.atelier3java.service.CardService;
import ics.atelier3java.common.dto.CardDTO;
import ics.atelier3java.common.dto.UserDTO;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping()
public class CardRest {
	
	@Autowired
	private CardService cardService;
	
	@PostMapping("/random/{nb}")
	public List<CardDTO> generateRandomCards(@RequestBody UserDTO user, @PathVariable int nb) {
		List<CardDTO> cards = new ArrayList<CardDTO>();
		for (Card card : cardService.generateRandomCards(nb)) {
			cards.add(card.toDTO());
		}
		return cards;
	}
	
	@GetMapping("/{id}")
	public CardDTO getById(@PathVariable int id) {
		if (cardService.getById(id) != null) {
			return cardService.getById(id).toDTO();
		} else {
			return null;
		}
	}
	
	@PutMapping("/{id}")
	public CardDTO updateById(@RequestBody Card card) {
		Card uCard = cardService.updateById(card);
		if (uCard != null) {
			return uCard.toDTO();
		} else {
			return null;
		}
	}
	
	@PostMapping("/addCard")
	public CardDTO createCard(@RequestBody Card Card) {
		return cardService.createCard(Card).toDTO();
	}
	
	@GetMapping("/list")
	public List<CardDTO> getCards() {
		List<CardDTO> cards = new ArrayList<CardDTO>();
		for (Card card : cardService.getCards()) {
			cards.add(card.toDTO());
		}
		return cards;
	}
	
	@GetMapping("/selling")
	public List<CardDTO> getCardsSelling() {
		return cardService.getCardsSelling();
	}
}
