package ics.atelier3java.model;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import ics.atelier3java.api.API;
import ics.atelier3java.api.DataAPICard;
import ics.atelier3java.common.dto.CardDTO;

@Entity
@Table(name = "Card")
public class Card {

		@Id
		@GeneratedValue
		private int id;
		private boolean selling;	
		private int price;
		private String name;
		@Column( length = 100000 )
		private String description;
		private int attack;
		private int defense;
		private int hp;
		private String image;
		private boolean inRoom;
		public int energy;
		
		public Card() {
			super();
		}
		
		public Card(int id, boolean selling, int price, String name, String description, int attack, int defense, int hp,
				String image, boolean inRoom, int energy) {
			super();
			this.id = id;
			this.selling = selling;
			this.price = price;
			this.name = name;
			this.description = description;
			this.attack = attack;
			this.defense = defense;
			this.hp = hp;
			this.image = image;
			this.inRoom = inRoom;
			this.energy = energy;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public boolean isSelling() {
			return selling;
		}

		public void setSelling(boolean selling) {
			this.selling = selling;
		}

		public int getPrice() {
			return price;
		}

		public void setPrice(int price) {
			this.price = price;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

		public int getAttack() {
			return attack;
		}

		public void setAttack(int attack) {
			this.attack = attack;
		}

		public int getDefense() {
			return defense;
		}

		public void setDefense(int defense) {
			this.defense = defense;
		}

		public int getHp() {
			return hp;
		}

		public void setHp(int hp) {
			this.hp = hp;
		}
		
		public boolean isInRoom() {
			return inRoom;
		}

		public void setInRoom(boolean inRoom) {
			this.inRoom = inRoom;
		}

		public int getEnergy() {
			return energy;
		}

		public void setEnergy(int energy) {
			this.energy = energy;
		}
		
		public CardDTO toDTO() {
			return new CardDTO(this.id, this.selling, this.price, this.name, this.description, this.attack, this.defense, this.hp,
					this.image, this.inRoom, this.energy, null);
		}
		
		public static Card generateCard() {
			DataAPICard apiCard = API.getInstance().getRandomCard();
			Card card = new Card();
			card.setName(apiCard.getName());
			card.setDescription(apiCard.getLore());
			Map<String, Object> info = apiCard.getInfo();
			card.setAttack((int) info.get("attack"));
			card.setDefense((int) info.get("defense"));
			Map<String, Object> stats = apiCard.getStats();
			card.setHp((int) stats.get("hp"));
			card.setImage("http://ddragon.leagueoflegends.com/cdn/img/champion/loading/" + apiCard.getId() + "_0.jpg");
			card.setSelling(false);
			card.setEnergy(100);
			card.setInRoom(false);
			return card;
		}
}
