package ics.atelier3java.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ics.atelier3java.common.dto.CardDTO;
import ics.atelier3java.common.dto.UserDTO;
import ics.atelier3java.service.MarketService;

@RestController
@RequestMapping()
public class MarketRest {
	
	@Autowired
	private MarketService marketService;
	
	@PostMapping("/sell/{price}")
	public boolean sell(@PathVariable Integer price, @RequestBody CardDTO card) {
		return marketService.sell(price, card);
	}
	
	@PostMapping("/cancelSell")
	public boolean cancelSell(@RequestBody CardDTO card) {
		return marketService.cancelSell(card);
	}
	
	@PostMapping("/buy/{cardId}")
	public boolean buy(@PathVariable Integer cardId, @RequestBody UserDTO user) {
		return marketService.buy(user, cardId);
	}
}
