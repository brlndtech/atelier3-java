package ics.atelier3java.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ics.atelier3java.common.dto.CardDTO;
import ics.atelier3java.common.dto.UserDTO;

@RestController
public class MarketService {
	
	public boolean sell(int price, CardDTO card) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity("http://card:8080/" + card.getId(), String.class);
		ObjectMapper mapper = new ObjectMapper();
		CardDTO apiCard = null;
		try {
			apiCard = mapper.readValue(response.getBody(), new TypeReference<CardDTO>(){});
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (apiCard != null && !card.isSelling() && !card.isInRoom()) {
			apiCard.setSelling(true);
			apiCard.setPrice(price);
			String jsonInString = "";
			try {
				jsonInString = mapper.writeValueAsString(apiCard);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> request = new HttpEntity<String>(jsonInString, headers);
			restTemplate.put("http://card:8080/" + card.getId(), request);
			return true;
		} else {
			return false;
		}
	}
	
	public boolean cancelSell(CardDTO card) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity("http://card:8080/" + card.getId(), String.class);
		ObjectMapper mapper = new ObjectMapper();
		CardDTO apiCard = null;
		try {
			apiCard = mapper.readValue(response.getBody(), new TypeReference<CardDTO>(){});
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (apiCard != null && card.isSelling()) {
			apiCard.setSelling(false);
			apiCard.setPrice(0);
			String jsonInString = "";
			try {
				jsonInString = mapper.writeValueAsString(apiCard);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> request = new HttpEntity<String>(jsonInString, headers);
			restTemplate.put("http://card:8080/" + card.getId(), request);
			return true;
		} else {
			return false;
		}
	}
	
	public boolean buy(UserDTO user, Integer cardId) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity("http://card:8080/" + cardId, String.class);
		ObjectMapper mapper = new ObjectMapper();
		CardDTO card = null;
		try {
			card = mapper.readValue(response.getBody(), new TypeReference<CardDTO>(){});
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (card != null) {
			response = restTemplate.getForEntity("http://user:8080/" + user.getId(), String.class);
			mapper = new ObjectMapper();
			UserDTO userBuy = null;
			try {
				userBuy = mapper.readValue(response.getBody(), new TypeReference<UserDTO>(){});
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (userBuy != null) {
				restTemplate = new RestTemplate();
				response = restTemplate.getForEntity("http://usercard:8080/card/" + card.getId(), String.class);
				mapper = new ObjectMapper();
				UserDTO userOwner = null;
				try {
					userOwner = mapper.readValue(response.getBody(), new TypeReference<UserDTO>(){});
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (userOwner != null && userBuy.getMoney() >= card.getPrice() && card.isSelling()) {
					userOwner.setMoney(userOwner.getMoney() + card.getPrice());
					userBuy.setMoney(userBuy.getMoney() - card.getPrice());
					card.setSelling(false);
					card.setPrice(0);
					String jsonInString = "";
					try {
						jsonInString = mapper.writeValueAsString(userBuy);
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
					HttpHeaders headers = new HttpHeaders();
				    headers.setContentType(MediaType.APPLICATION_JSON);
					HttpEntity<String> request = new HttpEntity<String>(jsonInString, headers);
					restTemplate.put("http://user:8080/" + userBuy.getId(), request);
					try {
						jsonInString = mapper.writeValueAsString(userOwner);
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
					request = new HttpEntity<String>(jsonInString, headers);
					restTemplate.put("http://user:8080/" + userOwner.getId(), request);
					try {
						jsonInString = mapper.writeValueAsString(card);
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
					request = new HttpEntity<String>(jsonInString, headers);
					restTemplate.put("http://card:8080/" + card.getId(), request);
					try {
						jsonInString = mapper.writeValueAsString(card);
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
					request = new HttpEntity<String>(jsonInString, headers);
					restTemplate.postForEntity("http://usercard:8080/" + card.getId() + "/" + userBuy.getId(), request, String.class);
					return true;
				}
			}
		}
		return false;
	}
}

